<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PengaduanController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UsersController;


use App\Http\Controllers\RoleController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\UserRoleController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('home', [HomeController::class, 'index'])->name('home');

Route::middleware('auth')->group(function () {
    Route::middleware('role:masyarakat')->group(function () {
        // Home Login
        Route::controller(HomeController::class)->group(function () {
            Route::get('/home/pengaduan', 'pengaduan')->name('home.pengaduan');
            Route::get('/home/{pengaduan}/show', 'show')->name('home.show');
            Route::get('/home/create', 'create')->name('home.create');
            Route::post('/home/store', 'store')->name('home.store');
        });
    });
    Route::middleware('role:topadmin|petugas')->group(function () {
        // DASHBOARD
        Route::controller(DashboardController::class)->group(function () {
            Route::get('/dashboard', 'index')->name('dashboard');
        });
        Route::controller(PengaduanController::class)->group(function () {
            Route::get('/pengaduan', 'index')->name('pengaduan.list');
            Route::get('/pengaduan/verifpengaduan', 'verifpengaduan')->name('pengaduan.verifpengaduan');
            Route::get('/pengaduan/{pengaduan}/aksiVerif', 'aksiverif')->name('pengaduan.aksiverif');
            Route::get('/pengaduan/{pengaduan}/show', 'show')->name('pengaduan.show');
            Route::get('/pengaduan/{pengaduan}/edit', 'edit')->name('pengaduan.edit');
            Route::put('/pengaduan/update/{pengaduan}', 'update')->name('pengaduan.update');
            Route::delete('/pengaduan/{pengaduan}/destroy', 'destroy')->name('pengaduan.destroy');
        });
    });
    Route::middleware('role:topadmin')->group(function () {
        // USERS
        Route::controller(UsersController::class)->group(function () {
            Route::get('users', 'index')->name('users.list');
            Route::get('/users/create', 'create')->name('users.create');
            Route::get('/users/{users}/edit', 'edit')->name('users.edit');
            Route::post('/users/store', 'store')->name('users.store');
            Route::put('/users/update/{users}', 'update')->name('users.update');
            Route::put('/users/updatepassword/{users}', 'updatePassword')->name('users.updatePassword');
            Route::delete('/users/{users}/destroy', 'destroy')->name('users.destroy');
            Route::put('/users/{users}/changerole', 'changeRole')->name('users.changeRole');
        });
      
        Route::resource('role', \App\Http\Controllers\RoleController::class);
        Route::resource('permission', \App\Http\Controllers\PermissionController::class);
        Route::resource('user_role', \App\Http\Controllers\UserRoleController::class);
    });
});
