<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $defaultUserValue = [
            'email_verified_at' => now(),
            'password' => '$2y$10$PB6EjAmeegO/jm93bPKkBOX9fwspR7qZZvSBYCLVgrkBndq5YBUiC',
            'remember_token' => Str::random(10)
        ];

        $topadmin = User::create(array_merge([
            'name' => 'Administrator',
            'email' => 'admin@pengaduan.com',
            'nik' => '199832423423423234',
            'telp' => '08156234234234',
        ], $defaultUserValue));
        $petugas = User::create(array_merge([
            'name' => 'Petugas',
            'email' => 'petugas@pengaduan.com',
            'nik' => '199832423423423235',
            'telp' => '08109889723432',
        ], $defaultUserValue));
        $masyarakat = User::create(array_merge([
            'name' => 'Masyarakat',
            'email' => 'masyarakat@gmail.com',
            'nik' => '199832423423423236',
            'telp' => '081234234234234',
        ], $defaultUserValue));

        $roleAdmin = Role::create(['name' => 'topadmin']);
        $roleOperator = Role::create(['name' => 'petugas']);
        $roleMasyarakat = Role::create(['name' => 'masyarakat']);

        $permission = Permission::create(['name' => 'TopAdmin role']);
        $permission = Permission::create(['name' => 'Petugas role']);
        $permission = Permission::create(['name' => 'Masyarakat role']);

        $topadmin->assignRole('topadmin');
        $petugas->assignRole('petugas');
        $masyarakat->assignRole('masyarakat');

    }
}
