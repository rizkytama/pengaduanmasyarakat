<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengaduan', function (Blueprint $table) {
            $table->id();
            $table->dateTime('tgl_pengaduan');
            $table->bigInteger('user_id');
            $table->string('judul_pengaduan');
            $table->text('isi_pengaduan');
            $table->string('foto_pengaduan');
            $table->enum('status_pengaduan',['0','proses','selesai']);
            $table->dateTime('tgl_tanggapan')->nullable();
            $table->text('isi_tanggapan')->nullable();
            $table->bigInteger('petugas_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengaduan');
    }
};
