@extends('layouts.auth')
@section('section')
<section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-4 col-md-6 d-flex flex-column align-items-center justify-content-center">

        <div class="d-flex justify-content-center py-4">
          <a href="index.html" class="logo d-flex align-items-center w-auto">
            <img src="assets/img/logo.png" alt="">
            <center><span class="d-none d-lg-block">{{ env('NAMA_APP') }}</span></center>
          </a>
        </div><!-- End Logo -->

        <div class="card mb-3">

          <div class="card-body">

            <div class="pt-4 pb-2">
              <h5 class="card-title text-center pb-0 fs-4">Buat Akun Anda</h5>
              <p class="text-center small">Masukan data diri anda untuk membuat akun</p>
            </div>

            <form class="row g-3 needs-validation" action="{{ route('register') }}" method="POST" novalidate>
              @csrf
              <div class="col-12">
                <label for="nik" class="form-label">NIK</label>
                <input type="text" name="nik" value="{{ old('nik') }}"
                  class="form-control @error('nik') is-invalid @enderror" id="nik" required>
                @error('nik')
                <div class="invalid-feedback">Silahkan masukkan NIK Anda!</div>
                @enderror
              </div>
              <div class="col-12">
                <label for="name" class="form-label">Nama Lengkap</label>
                <input type="text" name="name" value="{{ old('name') }}"
                  class="form-control @error('name') is-invalid @enderror" id="name" required>
                @error('name')
                <div class="invalid-feedback">Silahkan masukkan Nama Lenkap Anda!</div>
                @enderror
              </div>
              <div class="col-12">
                <label for="telp" class="form-label">Nomor Telp</label>
                <input type="text" name="telp" value="{{ old('telp') }}"
                  class="form-control @error('telp') is-invalid @enderror" id="telp" required>
                @error('telp')
                <div class="invalid-feedback">Silahkan masukkan Nama Lenkap Anda!</div>
                @enderror
              </div>

              <div class="col-12">
                <label for="yourEmail" class="form-label">Email</label>
                <input type="email" name="email" value="{{ old('email') }}"
                  class="form-control @error('email') @enderror" id="yourEmail" required>
                @error('email')
                <div class="invalid-feedback">Silahkan masukkan Email Anda!</div>
                @enderror
              </div>

              <div class="col-12">
                <label for="yourPassword" class="form-label">Password</label>
                <input type="password" name="password" value="{{ old('password') }}"
                  class="form-control @error('password') is-invalid @enderror" id="yourPassword" required>
                @error('pasword')
                <div class="invalid-feedback">Silahkan masukkan password Anda!</div>
                @enderror
              </div>

              <div class="col-12">
                <label for="yourPassword" class="form-label">Konfirmasi Password</label>
                <input type="password" name="password_confirmation" class="form-control" id="yourPassword" required>
              </div>

              <!-- <div class="col-12">
                      <div class="form-check">
                        <input class="form-check-input" name="terms" type="checkbox" value="" id="acceptTerms" required>
                        <label class="form-check-label" for="acceptTerms">I agree and accept the <a href="#">terms and conditions</a></label>
                        <div class="invalid-feedback">You must agree before submitting.</div>
                      </div>
                    </div> -->
              <div class="col-12">
                <button class="btn btn-primary w-100" type="submit">Create Account</button>
              </div>
              <div class="col-12">
                <p class="small mb-0">Already have an account? <a href="{{ route('login') }}">Log in</a></p>
              </div>
            </form>

          </div>
        </div>

        <div class="credits">
          <!-- All the links in the footer should remain intact. -->
          <!-- You can delete the links only if you purchased the pro version. -->
          <!-- Licensing information: https://bootstrapmade.com/license/ -->
          <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/ -->
          Created by <a href="#">Riza Rachmadhani, S.Kom</a>
        </div>

      </div>
    </div>
  </div>

</section>
@endsection