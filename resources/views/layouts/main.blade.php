<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title> {{ $title }} - {{ env('NAMA_APP') }}</title>
    <meta content="{{ env('NAMA_APP') }}" name="description">
    <meta content="{{ env('NAMA_APP') }}" name="keywords">

    <!-- Favicons -->
    <link href="{{ asset('theme/assets/img/favicon.png') }}" rel="icon">
    <link href="{{ asset('theme/assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.gstatic.com" rel="preconnect">
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{ asset('theme/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    {{--
    <link href="{{ asset('theme/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet') }}"> --}}
    <link href="{{ url('/') }}/theme/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="{{ asset('theme/assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/assets/vendor/quill/quill.snow.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/assets/vendor/quill/quill.bubble.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/assets/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
    <!-- Datatabless -->
    <link href="{{ asset('theme/assets/vendor/simple-datatables/style.css') }}" rel="stylesheet">
    <link href="{{ asset('DataTables/css/dataTables.bootstrap5.min.css') }}" rel="stylesheet">
    <!-- Template Main CSS File -->
    <link href="{{ asset('theme/assets/css/style.css') }}" rel="stylesheet">
    <!-- Toastr -->
    <link rel="stylesheet" href="{{ asset('Toastr/toastr.min.css') }}">
    <!-- Select2 -->
    <link href="{{ asset('select2-bootstrap5/select2-41/css/select2.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet"
        href="{{ asset('select2-bootstrap5/theme-bootstrap5/select2-bootstrap-5-theme.min.css') }}" />
    <!-- SweetAlert -->
    <link href="{{ asset('sweetalert2-11/package/dist/sweetalert2.min.css') }}" rel="stylesheet" />

    <link href="https://cdn.datatables.net/datetime/1.2.0/css/dataTables.dateTime.min.css" rel="stylesheet" />

    @stack('styles')

    <!-- =======================================================
  * Template Name: NiceAdmin - v2.5.0
  * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

    <!-- ======= Header ======= -->
    <header id="header" class="header fixed-top d-flex align-items-center">

        <div class="d-flex align-items-center justify-content-between">
            <a href="{{ route('dashboard') }}" class="logo d-flex align-items-center">
                <img src="{{ asset('theme/assets/img/logo.png') }}" alt="">
                <span class="d-none d-lg-block">{{ env('NAMA_APP_SHORT') }}</span>
            </a>
            <i class="bx bx-menu toggle-sidebar-btn"></i>
        </div><!-- End Logo -->

        {{-- <div class="search-bar">
            <form class="search-form d-flex align-items-center" method="POST" action="#">
                <input type="text" name="query" placeholder="Search" title="Enter search keyword">
                <button type="submit" title="Search"><i class="bi bi-search"></i></button>
            </form>
        </div>
        <!-- End Search Bar --> --}}

        <nav class="header-nav ms-auto">
            <ul class="d-flex align-items-center">

                <li class="nav-item d-block d-lg-none">
                    <a class="nav-link nav-icon search-bar-toggle " href="#">
                        <i class="bi bi-search"></i>
                    </a>
                </li><!-- End Search Icon-->

                <li class="nav-item dropdown pe-3">

                    <a class="nav-link nav-profile d-flex align-items-center pe-0" href="#" data-bs-toggle="dropdown">
                        <img src="{{ asset('theme/assets/img/profile-img.jpg') }}" alt="Profile" class="rounded-circle">
                        <span class="d-none d-md-block dropdown-toggle ps-2">{{ auth()->user()->name }}</span>
                    </a><!-- End Profile Iamge Icon -->

                    <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">
                        <li class="dropdown-header">
                            <h6>{{ auth()->user()->name }}</h6>
                            <span>{{ auth()->user()->instansi->nama_instansi??"Top Admin e-Retribusi" }}</span>
                            <p style="margin-bottom:-5px">
                                @forelse(auth()->user()->roles()->pluck('name') as $rolenya)
                                {{ ucfirst($rolenya) }} &nbsp;
                                @empty
                                tidak ada role
                                @endforelse
                            </p>
                        </li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>

                        {{-- <li>
                            <a class="dropdown-item d-flex align-items-center" href="users-profile.html">
                                <i class="bi bi-person"></i>
                                <span>My Profile</span>
                            </a>
                        </li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>

                        <li>
                            <a class="dropdown-item d-flex align-items-center" href="users-profile.html">
                                <i class="bi bi-gear"></i>
                                <span>Account Settings</span>
                            </a>
                        </li> --}}
                        <li>
                            <a class="dropdown-item d-flex align-items-center text-danger" href="{{ route('logout') }}"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="bi bi-box-arrow-right"></i>
                                <span>Sign Out</span>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                @csrf
                            </form>
                        </li>

                    </ul><!-- End Profile Dropdown Items -->
                </li><!-- End Profile Nav -->

            </ul>
        </nav><!-- End Icons Navigation -->

    </header><!-- End Header -->

    @include('partials.sidebar')

    <main id="main" class="main">

        <div class="pagetitle">
            <h1>{!!$icon!!} {{$title}}</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                    <li class="breadcrumb-item active">{{$title}}</li>
                </ol>
            </nav>
        </div><!-- End Page Title -->
        @yield('section')
    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    <footer id="footer" class="footer">
        <div class="copyright">
            &copy; Copyright <strong><span>{{ env('NAMA_APP') }}</span></strong>. All Rights Reserved
        </div>
        <div class="credits">
            <!-- All the links in the footer should remain intact. -->
            <!-- You can delete the links only if you purchased the pro version. -->
            <!-- Licensing information: https://bootstrapmade.com/license/ -->
            <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/ -->
            Dinas <a href="https://bootstrapmade.com/">Kominfo Mojokerto</a>
        </div>
    </footer><!-- End Footer -->

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{ asset('theme/assets/vendor/apexcharts/apexcharts.min.js') }}"></script>
    <script src="{{ asset('theme/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('theme/assets/vendor/chart.js/chart.umd.js') }}"></script>
    <script src="{{ asset('theme/assets/vendor/echarts/echarts.min.js') }}"></script>
    <script src="{{ asset('theme/assets/vendor/quill/quill.min.js') }}"></script>
    <script src="{{ asset('theme/assets/vendor/simple-datatables/simple-datatables.js') }}"></script>
    <script src="{{ asset('theme/assets/vendor/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('theme/assets/vendor/php-email-form/validate.js') }}"></script>
    <!-- Datatabless -->
    <script src="{{ asset('DataTables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('DataTables/js/dataTables.bootstrap5.min.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.2/moment.min.js"></script>
    <script src="https://cdn.datatables.net/datetime/1.2.0/js/dataTables.dateTime.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.13.1/dataRender/datetime.js"></script>

    <!-- Template Main JS File -->
    <script src="{{ asset('theme/assets/js/main.js') }}"></script>
    <!-- Toastr -->
    <script src="{{ asset('Toastr/toastr.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('select2-bootstrap5/select2-41/js/select2.min.js') }}"></script>
    <!-- SweetAlert -->
    <script src="{{ asset('sweetalert2-11/package/dist/sweetalert2.all.min.js') }}"></script>
    <script>
        //message with toastr
        @if(session() -> has('success'))
        toastr.success('{{ session('success') }}', 'BERHASIL!');

        @elseif(session() -> has('error'))

        toastr.error('{{ session('error') }}', 'GAGAL!');
        @endif
    </script>
    <script>
        $(document).ready(function () {
            // select2 for class select2
            $('.select2').select2({
                theme: "bootstrap-5",
                width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ?
                    '100%' : 'style',
                placeholder: $(this).data('placeholder'),
                allowClear: true
            });
        });
        const loadingStart = ()=>{
            Swal.fire({
                title: 'Sedang Diproses...',
                html: 'Mohon Tunggu, Data Anda Sedang Diproses',
                allowOutsideClick:false,
                didOpen: () => {
                    Swal.showLoading()
                }
            })
        }
    </script>

    @stack('scripts')

</body>

</html>