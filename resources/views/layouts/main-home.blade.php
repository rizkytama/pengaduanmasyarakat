@php
if(Auth::check()){
if (auth()->user()->hasRole('topadmin')) {

}
}
@endphp
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>{{ env('NAMA_APP_SHORT') }} - {{ env('NAMA_APP') }}</title>
    <meta content="{{ env('NAMA_APP') }}" name="description">
    <meta content="{{ env('NAMA_APP') }}" name="keywords">

    <!-- Favicons -->
    <link href="{{ asset('theme-home/assets/img/favicon.png') }}" rel="icon">
    <link href="{{ asset('theme-home/assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,600;1,700&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Inter:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet">
    <!-- Datatabless -->
    <link href="{{ asset('theme/assets/vendor/simple-datatables/style.css') }}" rel="stylesheet">
    <link href="{{ asset('DataTables/css/dataTables.bootstrap5.min.css') }}" rel="stylesheet">
    <!-- Vendor CSS Files -->
    <link href="{{ asset('theme-home/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme-home/assets/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('theme-home/assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme-home/assets/vendor/glightbox/css/glightbox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme-home/assets/vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme-home/assets/vendor/aos/aos.css') }}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{ asset('theme-home/assets/css/main.css') }}" rel="stylesheet">

    <!-- =======================================================
  * Template Name: Logis - v1.3.0
  * Template URL: https://bootstrapmade.com/logis-bootstrap-logistics-website-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>
    <!-- ======= Header ======= -->
    <header id="header" class="header d-flex align-items-center fixed-top">
        <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

            <a href="index.html" class="logo d-flex align-items-center">
                <!-- Uncomment the line below if you also wish to use an image logo -->
                <!-- <img src="{{ asset('theme-home/assets/img/logo.png') }}" alt=""> -->
                <h1>{{ env('NAMA_APP_SHORT') }}</h1>
            </a>

            <i class="mobile-nav-toggle mobile-nav-show bi bi-list"></i>
            <i class="mobile-nav-toggle mobile-nav-hide d-none bi bi-x"></i>
            <nav id="navbar" class="navbar">
                <ul>
                    @if(Auth::check())
                    <li><a class="" href="{{ route('home') }}">Home</a></li>
                    <li><a class="" href="{{ route('home.pengaduan') }}">Pengaduan</a></li>
                    <li><a class="get-a-quote bg-danger" href="{{ route('logout') }}"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <span>Sign Out</span>
                        </a></li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST">
                        @csrf
                    </form>
                    @else
                    <li><a class="get-a-quote" href="{{ route('login') }}">Login</a></li>
                    @endif
                </ul>
            </nav><!-- .navbar -->

        </div>
    </header><!-- End Header -->
    <!-- End Header -->




    @yield('section')

    <!-- ======= Footer ======= -->
    <footer id="footer" class="footer">



        <div class="container mt-4">
            <div class="copyright">
                &copy; Copyright <strong><span>{{ env('NAMA_APP_SHORT') }}</span></strong>. All Rights Reserved
            </div>
            <div class="credits">
                <!-- All the links in the footer should remain intact. -->
                <!-- You can delete the links only if you purchased the pro version. -->
                <!-- Licensing information: https://bootstrapmade.com/license/ -->
                <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/logis-bootstrap-logistics-website-template/ -->
                Create by <a href="#">Riza Rachmadhani, S.Kom</a>
            </div>
        </div>

    </footer><!-- End Footer -->
    <!-- End Footer -->

    <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <div id="preloader"></div>

    <!-- Vendor JS Files -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{ asset('theme-home/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('theme-home/assets/vendor/purecounter/purecounter_vanilla.js') }}"></script>
    <script src="{{ asset('theme-home/assets/vendor/glightbox/js/glightbox.min.js') }}"></script>
    <script src="{{ asset('theme-home/assets/vendor/swiper/swiper-bundle.min.js') }}"></script>
    <script src="{{ asset('theme-home/assets/vendor/aos/aos.js') }}"></script>
    <script src="{{ asset('theme-home/assets/vendor/php-email-form/validate.js') }}"></script>

    <!-- Datatabless -->
    <script src="{{ asset('DataTables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('DataTables/js/dataTables.bootstrap5.min.js') }}"></script>

    <!-- Template Main JS File -->
    <script src="{{ asset('theme-home/assets/js/main.js') }}"></script>

    @stack('scripts')

</body>

</html>