@extends('layouts.main')
@section('section')
<div class="card">
    <div class="card-header"><b><i class="bi bi-layout-text-window-reverse"></i> Formulir {{$title}}</b></div>
    <div class="card-body mt-3">
        <table class="table">
            <tr>
                <th>STATUS PENGADUAN</th>
                <th>:</th>
                <td>
                    @if($pengaduan->status_pengaduan =='0')
                    <span class="badge bg-secondary">Terkirim</span>
                    @elseif($pengaduan->status_pengaduan =='proses')
                    <span class="badge bg-warning">Proses</span>
                    @elseif($pengaduan->status_pengaduan =='selesai')
                    <span class="badge bg-success">Selesai</span>
                    @else
                    <span class="badge bg-secondary">Tidak Diketahui</span>
                    @endif
                </td>
            </tr>
            <tr>
                <th>TANGGAL PENGADUAN</th>
                <th>:</th>
                <td>{{ $pengaduan->tgl_pengaduan }}</td>
            </tr>
            <tr>
                <th>JUDUL PENGADUAN</th>
                <th>:</th>
                <td>{{ $pengaduan->judul_pengaduan }}</td>
            </tr>
            <tr>
                <th>ISI PENGADUAN</th>
                <th>:</th>
                <td>{{ $pengaduan->isi_pengaduan }}</td>
            </tr>
            <tr>
                <th>FOTO PENGADUAN</th>
                <th>:</th>
                <td><img src="{{ url('storage/foto_pengaduan').'/'.$pengaduan->foto_pengaduan }}" alt=""></td>
            </tr>
        </table>
        <!-- Horizontal Form -->
        <form action="{{ route('pengaduan.update',$pengaduan->id) }}" method="POST" enctype="multipart/form-data">

            @csrf
            @method('PUT')

            <div class="row mb-3">
                <label for="isi_tanggapan" class="col-sm-2 col-form-label">Tanggapan</label>
                <div class="col-sm-10">
                    <textarea type="text" class="form-control @error('isi_tanggapan') is-invalid @enderror"
                        name="isi_tanggapan" placeholder="Masukkan Tanggapan Dari Aduan Diatas"
                        id="isi_tanggapan">{{ old('isi_tanggapan',$pengaduan->isi_tanggapan) }}</textarea>
                    @error('isi_tanggapan')
                    <div class="alert alert-danger mt-2">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>

            <div class="text-center">
                <button type="submit" class="btn btn-primary"><i class="bi bi-sd-card"></i> Simpan</button>
                <a href="{{route('pengaduan.list')}}" class="btn btn-secondary"><i class="bi bi-x-lg"></i> Cancel</a>
            </div>
        </form><!-- End Horizontal Form -->

    </div>
</div>
@endsection