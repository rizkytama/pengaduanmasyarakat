@extends('layouts.main')
@section('section')
<section class="section dashboard">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-4">
                    <b><i class="bi bi-table"></i> {{$title}}</b>
                </div>
                <div class="col-md-4 text-center">
                    <div style="margin-top: 4px" id="message">
                    </div>
                </div>
                <div class="col-md-4 text-end">
                </div>
            </div>
        </div>
        <div class="card-body mt-3">
            <div class="table-responsive">
                <table class="table table-striped" id="dataTable">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Foto</th>
                            <th scope="col">Tanggal</th>
                            <th scope="col">Judul</th>
                            <th scope="col">Status</th>
                            <th scope="col" class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<input type="hidden" value="{{ route('pengaduan.list') }}" id="urlCurrent">
<input type="hidden" value="{{ route('pengaduan.edit',':slug') }}" id="urlVerif">
@endsection

@push('scripts')
<script type="text/javascript">
    let urlCurrent=$('#urlCurrent').val();
    let urlVerif=$('#urlVerif').val();
    let urlShow=$('#urlShow').val();
    $(document).ready(function () {
        $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: urlCurrent,
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
                {
                    data: 'foto_pengaduan',
                    render:function(data,type,row){
                        return '<img src="'+data+'" class="rounded" style="width: 50px">';
                    }
                },
                {
                    data: 'tgl_pengaduan',
                },
                {
                    data: 'judul_pengaduan',
                },
                {
                    data: 'status_pengaduan',
                    render:function (data, type, row) {
                        if(data =='0'){
                            roles='<span class="badge bg-secondary">Terkirim</span>';   
                        }else if(data =='proses'){
                            roles='<span class="badge bg-warning">Proses</span>';   
                        }else if(data =='selesai'){
                            roles='<span class="badge bg-success">Selesai</span>';   
                        }else{
                            roles='<span class="badge bg-secondary">Tidak Diketahui</span>';   
                        }
                        
                        return roles+'<p><i>'+row.tgl_pengaduan+'</i></p>';
                    }
                },
                {
                    data: 'id',name: 'id',orderable: false, searchable: false,
                    render:function (data, type, row) {
                        linkVerif = urlVerif.replace(':slug', data);
                        return '<center><a href="'+linkVerif+'" class="btn btn-sm btn-primary"><i class="bx bx-check"></i> Tanggapi</a></center>';
                    }
                }

            ]
        });
    });
 

</script>
@endpush