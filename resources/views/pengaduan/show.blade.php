@extends('layouts.main')
@section('section')
<section id="contact" class="contact">
    <div class="container" data-aos="fade-up">

        <div class="row gy-4 mt-4">

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        PENGADUAN
                    </div>
                    <div class="card-body mt-3">
                        <table class="table">
                            <tr>
                                <th>STATUS PENGADUAN</th>
                                <th>:</th>
                                <td>
                                    @if($pengaduan->status_pengaduan =='0')
                                    <span class="badge bg-secondary">Terkirim</span>
                                    @elseif($pengaduan->status_pengaduan =='proses')
                                    <span class="badge bg-warning">Proses</span>
                                    @elseif($pengaduan->status_pengaduan =='selesai')
                                    <span class="badge bg-success">Selesai</span>
                                    @else
                                    <span class="badge bg-secondary">Tidak Diketahui</span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>TANGGAL PENGADUAN</th>
                                <th>:</th>
                                <td>{{ $pengaduan->tgl_pengaduan }}</td>
                            </tr>
                            <tr>
                                <th>JUDUL PENGADUAN</th>
                                <th>:</th>
                                <td>{{ $pengaduan->judul_pengaduan }}</td>
                            </tr>
                            <tr>
                                <th>ISI PENGADUAN</th>
                                <th>:</th>
                                <td>{{ $pengaduan->isi_pengaduan }}</td>
                            </tr>
                            <tr>
                                <th>FOTO PENGADUAN</th>
                                <th>:</th>
                                <td><img src="{{ url('storage/foto_pengaduan').'/'.$pengaduan->foto_pengaduan }}"
                                        alt=""></td>
                            </tr>
                        </table>
                        <a href="{{ route('pengaduan.aksiverif',$pengaduan->id) }}" class="btn btn btn-primary"><i
                                class="bx bx-check"></i> Verif Pengaduan</a>
                        <a href="{{route('pengaduan.verifpengaduan')}}" class="btn btn-secondary"><i
                                class="bi bi-x-lg"></i>
                            Kembali</a>
                    </div>
                </div>

            </div><!-- End Contact Form -->

        </div>

    </div>
</section><!-- End Contact Section -->

@endsection

@push('scripts')
<script>

</script>
@endpush