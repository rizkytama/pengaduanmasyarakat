<!-- ======= Sidebar ======= -->
<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">
        <li class="nav-heading">Dashboard</li>
        <li class="nav-item">
            <a class="nav-link {{ $active == 'dashboard' ? '' : 'collapsed' }}" href="{{route('dashboard')}}">
                <i class="bx bx-grid-alt"></i>
                <span>Dashboard </span>
            </a>
        </li><!-- End Dashboard Nav -->
        <li class="nav-heading">Pengaduan</li>
        <li class="nav-item">
            <a class="nav-link {{ $active == 'verifdatapengaduan' ? '' : 'collapsed' }}"
                href="{{route('pengaduan.verifpengaduan')}}">
                <i class="bx bx-cart-alt"></i>
                <span>Verifikasi Pengaduan</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ $active == 'datapengaduan' ? '' : 'collapsed' }}" href="{{route('pengaduan.list')}}">
                <i class="bx bx-cart-alt"></i>
                <span>Tanggapi Pengaduan</span>
            </a>
        </li>

        <li class="nav-heading">Laporan Pengaduan</li>
        <li class="nav-item">
            <a class="nav-link {{ $active == 'laporanpengaduan' ? '' : 'collapsed' }}" href="{{route('dashboard')}}">
                <i class="bx bx-cart-alt"></i>
                <span>Generate Laporan</span>
            </a>
        </li>
        @role('topadmin')
        {{-- <li class="nav-heading">Pengaturan Data</li>
        <li class="nav-item">
            <a class="nav-link {{ ($active === 'PengaturanItem') ? '' : 'collapsed' }}" href="{{route('item.list')}}">
                <i class="bx bx-dollar-circle"></i>
                <span>Item Barang / Jasa</span>
            </a>
        </li>

        <li class="nav-heading">Referensi Data</li>
        <li class="nav-item">
            <a class="nav-link {{ $active == 'datakategori' ? '' : 'collapsed' }}" href="{{route('kategori.list')}}">
                <i class="bx bx-book-content"></i>
                <span>Data Kategori</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ ($active === 'datasubkategori') ? '' : 'collapsed' }}"
                href="{{route('subkategori.list')}}">
                <i class="bx bx-book-content"></i>
                <span>Data Sub Kategori</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ ($active === 'datainstansi') ? '' : 'collapsed' }}" href="{{route('instansi.list')}}">
                <i class="bx bx-home-circle"></i>
                <span>Data Instansi</span>
            </a>
        </li> --}}
        <li class="nav-heading">Users</li>
        <li class="nav-item">
            <a class="nav-link {{ ($active === 'users') ? '' : 'collapsed' }}" href="{{route('users.list')}}">
                <i class="bx bx-user-circle"></i>
                <span>Data Users</span>
            </a>
        </li>
        @endrole
    </ul>

</aside><!-- End Sidebar-->