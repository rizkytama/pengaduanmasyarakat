@extends('layouts.main')
@section('section')
<section class="section dashboard">
    <div class="row">

        <!-- Left side columns -->
        <div class="col-lg-12">
            <div class="row">

                <!-- Sales Card -->
                <div class="col-xxl-4 col-md-6">
                    <div class="card info-card sales-card">

                        <div class="card-body">
                            <h5 class="card-title">Pengaduan Yang Belum Ditanggapi <span></span></h5>

                            <div class="d-flex align-items-center">
                                <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                    <i class="bx bx-cart-alt"></i>
                                </div>
                                <div class="ps-3">
                                    <div id="jmlTransaksi">
                                        <div class="spinner-border text-primary" role="status">
                                            <span class="visually-hidden">Loading...</span>
                                        </div>
                                    </div>
                                    <span class="text-muted small pt-2 ps-1">Total Pengaduan Yang <span
                                            class="text-danger small pt-1 fw-bold">Belum</span> Ditanggapi</span>

                                </div>
                            </div>
                        </div>

                    </div>
                </div><!-- End Sales Card -->

                <!-- Revenue Card -->
                <div class="col-xxl-4 col-md-6">
                    <div class="card info-card revenue-card">

                        <div class="card-body">
                            <h5 class="card-title">Pengaduan Yang Sudah Ditanggapi <span></span></h5>

                            <div class="d-flex align-items-center">
                                <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                    <i class="bx bx-money"></i>
                                </div>
                                <div class="ps-3">
                                    <div id="totalRetribusi">
                                        <div class="spinner-border text-primary" role="status">
                                            <span class="visually-hidden">Loading...</span>
                                        </div>
                                    </div>
                                    <span class="text-muted small pt-2 ps-1">Total Pengaduan Yang <span
                                            class="text-success small pt-1 fw-bold">Sudah</span> Ditanggapi</span>

                                </div>
                            </div>
                        </div>

                    </div>
                </div><!-- End Revenue Card -->

            </div>
        </div><!-- End Left side columns -->

    </div>
</section>

@endsection

@push('scripts')
<script>

</script>
@endpush