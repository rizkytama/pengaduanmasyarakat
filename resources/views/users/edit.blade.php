@extends('layouts.main')
@section('section')
<section class="section profile">
    <div class="row">
        <div class="col-xl-4">

            <div class="card">
                <div class="card-body profile-card pt-4 d-flex flex-column align-items-center">

                    <img src="{{ asset('theme/assets/img/profile-img.jpg') }}" alt="Profile" class="rounded-circle">
                    <h2 class="text-center">{{ $user->name }}</h2>
                    <h3 class="text-center">{{ $user->instansi->nama_instansi??'' }}</h3>
                    <div class="social-links mt-2">
                        <p><i class="bx bx-envelope"></i> {{ $user->email }}</p>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-xl-8">

            <div class="card">
                <div class="card-body pt-3">
                    <!-- Bordered Tabs -->
                    <ul class="nav nav-tabs nav-tabs-bordered">

                        {{-- <li class="nav-item">
                            <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#profile-edit">Edit
                                Profile</button>
                        </li>

                        <li class="nav-item">
                            <button class="nav-link" data-bs-toggle="tab"
                                data-bs-target="#profile-change-password">Change Password</button>
                        </li>
                        <li class="nav-item">
                            <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-role">Role</button>
                        </li> --}}

                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="profile-edit-tab" data-bs-toggle="tab"
                                data-bs-target="#profile-edit" type="button" role="tab" aria-controls="profile-edit"
                                aria-selected="true">Ubah
                                Profile</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="profile-change-password-tab" data-bs-toggle="tab"
                                data-bs-target="#profile-change-password" type="button" role="tab"
                                aria-controls="profile-change-password" aria-selected="false">Ubah Password</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="profile-role-tab" data-bs-toggle="tab"
                                data-bs-target="#profile-role" type="button" role="tab" aria-controls="profile-role"
                                aria-selected="false">Role</button>
                        </li>

                    </ul>
                    <div class="tab-content pt-2">

                        <div class="tab-pane fade show active profile-edit pt-3" id="profile-edit">

                            <!-- Profile Edit Form -->
                            <form action="{{ route('users.update', $user->id) }}" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="row mb-3">
                                    <label for="name" class="col-sm-2 col-form-label">Nama</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control @error('name') is-invalid @enderror"
                                            name="name" value="{{ old('name',$user->name) }}"
                                            placeholder="Masukkan Nama User" id="name">
                                        @error('name')
                                        <div class="alert alert-danger mt-2">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="email" class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control @error('email') is-invalid @enderror"
                                            name="email" value="{{ old('email',$user->email) }}"
                                            placeholder="Masukkan Email User" id="email">
                                        @error('email')
                                        <div class="alert alert-danger mt-2">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary"><i class="bi bi-sd-card"></i>
                                        Simpan</button>
                                    <a href="{{route('users.list')}}" class="btn btn-secondary"><i
                                            class="bi bi-x-lg"></i> Cancel</a>
                                </div>
                            </form><!-- End Horizontal Form -->

                        </div>
                        <div class="tab-pane fade show  profile-role pt-3" id="profile-role">
                            <form action="{{ route('users.changeRole', $user->id) }}" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                @forelse($roles as $role)
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="role" id="{{ $role->name }}"
                                        value="{{ $role->name }}" @if($roleUser->contains('name',$role->name))checked
                                    @endif

                                    >
                                    <label class="form-check-label" for="{{ $role->name }}">
                                        {{ ucfirst($role->name) }}
                                    </label>
                                </div>
                                @empty
                                <p>Data Roles Kosong</p>
                                @endforelse


                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary"><i class="bi bi-sd-card"></i>
                                        Simpan</button>
                                    <a href="{{route('users.list')}}" class="btn btn-secondary"><i
                                            class="bi bi-x-lg"></i> Cancel</a>
                                </div>
                            </form><!-- End Change Password Form -->

                        </div>

                        <div class="tab-pane fade pt-3" id="profile-change-password">
                            <!-- Change Password Form -->
                            <form action="{{ route('users.updatePassword', $user->id) }}" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                @method('PUT')

                                <div class="row mb-3">
                                    <label for="password" class="col-sm-3 col-form-label">Password</label>
                                    <div class="col-sm-9">
                                        <input type="password"
                                            class="form-control @error('password') is-invalid @enderror" name="password"
                                            value="{{ old('password') }}" placeholder="Masukkan Password User"
                                            id="password">
                                        @error('password')
                                        <div class="alert alert-danger mt-2">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="password_confirmation" class="col-sm-3 col-form-label">Konfirmasi
                                        Password Baru</label>
                                    <div class="col-sm-9">
                                        <input type="password"
                                            class="form-control @error('password_confirmation') is-invalid @enderror"
                                            name="password_confirmation" value="{{ old('password_confirmation') }}"
                                            placeholder="Masukkan Kembali Password Baru User"
                                            id="password_confirmation">
                                        @error('password_confirmation')
                                        <div class="alert alert-danger mt-2">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>

                                    <div class="text-center">
                                        <button type="submit" class="btn btn-primary"><i class="bi bi-sd-card"></i>
                                            Simpan</button>
                                        <a href="{{route('users.list')}}" class="btn btn-secondary"><i
                                                class="bi bi-x-lg"></i> Cancel</a>
                                    </div>
                            </form><!-- End Change Password Form -->

                        </div>

                    </div><!-- End Bordered Tabs -->

                </div>
            </div>

        </div>
    </div>
</section>

@endsection