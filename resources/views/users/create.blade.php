@extends('layouts.main')
@section('section')
<div class="card">
    <div class="card-header"><b><i class="bi bi-layout-text-window-reverse"></i> Formulir {{$title}}</b></div>
    <div class="card-body mt-3">
        <!-- Horizontal Form -->
        <form action="{{ route('users.store') }}" method="POST" novalidate>

            @csrf
            <div class="row mb-3">
                <label for="nik" class="col-sm-2 col-form-label">NIK</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control @error('nik') is-invalid @enderror" name="nik"
                        value="{{ old('nik') }}" placeholder="Masukkan NIK " id="nik">
                    @error('nik')
                    <div class="alert alert-danger mt-2">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>
            <div class="row mb-3">
                <label for="name" class="col-sm-2 col-form-label">Nama</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name"
                        value="{{ old('name') }}" placeholder="Masukkan Nama " id="name">
                    @error('name')
                    <div class="alert alert-danger mt-2">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>
            <div class="row mb-3">
                <label for="telp" class="col-sm-2 col-form-label">Nomor Telp</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control @error('telp') is-invalid @enderror" name="telp"
                        value="{{ old('telp') }}" placeholder="Masukkan Nomor Telp" id="telp">
                    @error('telp')
                    <div class="alert alert-danger mt-2">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>
            <div class="row mb-3">
                <label for="email" class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                        value="{{ old('email') }}" placeholder="Masukkan Email " id="email">
                    @error('email')
                    <div class="alert alert-danger mt-2">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>
            <div class="row mb-3">
                <label for="password" class="col-sm-2 col-form-label">Password</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password"
                        value="{{ old('password') }}" placeholder="Masukkan Password " id="password">
                    @error('password')
                    <div class="alert alert-danger mt-2">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>
            <div class="row mb-3">
                <label for="password_confirmation" class="col-sm-2 col-form-label">Konfirmasi Password</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror"
                        name="password_confirmation" value="{{ old('password_confirmation') }}"
                        placeholder="Masukkan Kembali Password Baru User" id="password_confirmation">
                    @error('password_confirmation')
                    <div class="alert alert-danger mt-2">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-primary"><i class="bi bi-sd-card"></i> Simpan</button>
                <a href="{{route('users.list')}}" class="btn btn-secondary"><i class="bi bi-x-lg"></i> Cancel</a>
            </div>
        </form><!-- End Horizontal Form -->

    </div>
</div>
@endsection