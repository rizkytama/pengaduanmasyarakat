@extends('layouts.main')
@section('section')
<section class="section dashboard">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-4">
                    <b><i class="bi bi-table"></i> {{$title}}</b>
                </div>
                <div class="col-md-4 text-center">
                    <div style="margin-top: 4px" id="message">
                    </div>
                </div>
                <div class="col-md-4 text-end">
                    <a href="{{route('users.create')}}" class="btn btn-primary"><i class="bi bi-plus-circle"></i>
                        Tambah</a>
                </div>
            </div>
        </div>
        <div class="card-body mt-3">
            <div class="table-responsive">
                <table class="table table-striped" id="dataTable">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">NIK</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Telp</th>
                            <th scope="col">Email/Username</th>
                            <th scope="col">Role</th>
                            <th scope="col" class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<input type="hidden" value="{{ route('users.list') }}" id="urlCurrent">
<input type="hidden" value="{{ route('users.edit',':slug') }}" id="urlEdit">
<input type="hidden" value="{{ route('users.destroy',':slug') }}" id="urlDestroy">
@endsection

@push('scripts')
<script type="text/javascript">
    let urlCurrent=$('#urlCurrent').val();
    let urlEdit=$('#urlEdit').val();
    let urlDestroy=$('#urlDestroy').val();
    $(document).ready(function () {
        $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: urlCurrent,
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
                {
                    data: 'nik',
                },
                {
                    data: 'name',
                },
                {
                    data: 'telp',
                },
                {
                    data: 'email',
                },
                {
                    data: 'id',
                    render:function (data, type, row) {
                        let roles="";
                        if(row.roles.length > 0){
                            $.each(row.roles , function(index, val) { 
                                roles=roles+'<span class="badge bg-primary">'+val.name+'</span>';
                            });
                        }else{
                            roles='<span class="badge bg-danger">Role User Belum Diset</span>';   
                        }
                        
                        return roles;
                    }
                },
                {
                    data: 'id',name: 'id',orderable: false, searchable: false,
                    render:function (data, type, row) {
                        linkEdit = urlEdit.replace(':slug', data);
                        linkDestroy = urlDestroy.replace(':slug', data);
                        return '<form onsubmit="return confirm(`Apakah anda ingin menghapus data ini?`);" action="'+linkDestroy+'" method="POST"><a href="'+linkEdit+'" class="btn btn-sm btn-primary"><i class="bx bx-pencil"></i></a> @csrf @method("DELETE")<button type="submit" class="btn btn-sm btn-danger"><i class="bx bx-trash"></i></button></form>';
                    }
                }

            ]
        });
    });
 

</script>
@endpush