@extends('layouts.main-home')
@section('section')
<div class="breadcrumbs">
    <div class="page-header d-flex align-items-center"
        style="background-image: url('{{ asset('theme-home/assets/img/page-header.jpg') }}');">
        <div class="container position-relative">
            <div class="row d-flex justify-content-center">
                <div class="col-lg-6 text-center">
                    <h2>Menu Pengaduan</h2>
                    <p>Data Pengaduan Anda.</p>
                </div>
            </div>
        </div>
    </div>
    <nav>
        <div class="container">
            <ol>
                <li><a href="index.html">Home</a></li>
                <li>Data Pengaduan Anda</li>
            </ol>
        </div>
    </nav>
</div><!-- End Breadcrumbs -->

<!-- ======= Contact Section ======= -->
<section id="contact" class="contact">
    <div class="container" data-aos="fade-up">

        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-4">
                        <b><i class="bi bi-table"></i> Data Pengaduan</b>
                    </div>
                    <div class="col-md-4 text-center">
                        <div style="margin-top: 4px" id="message">
                        </div>
                    </div>
                    <div class="col-md-4 text-end">
                        <a href="{{route('home.create')}}" class="btn btn-primary"><i class="bi bi-plus-circle"></i>
                            Tambah</a>
                    </div>
                </div>
            </div>
            <div class="card-body mt-3">
                <div class="table-responsive">
                    <table class="table table-striped" id="dataTable">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Foto</th>
                                <th scope="col">Tanggal</th>
                                <th scope="col">Judul</th>
                                <th scope="col">Status</th>
                                <th scope="col" class="text-center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</section><!-- End Contact Section -->

<input type="hidden" value="{{ route('home.pengaduan') }}" id="urlCurrent">
<input type="hidden" value="{{ route('home.show',':slug') }}" id="urlShow">
@endsection

@push('scripts')
<script type="text/javascript">
    let urlCurrent=$('#urlCurrent').val();
    let urlShow=$('#urlShow').val();
    let urlDestroy=$('#urlDestroy').val();
    $(document).ready(function () {
        $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: urlCurrent,
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
                {
                    data: 'foto_pengaduan',
                    render:function(data,type,row){
                        return '<img src="'+data+'" class="rounded" style="width: 50px">';
                    }
                },
                {
                    data: 'tgl_pengaduan',
                },
                {
                    data: 'judul_pengaduan',
                },
                {
                    data: 'status_pengaduan',
                    render:function (data, type, row) {
                        if(data =='0'){
                            roles='<span class="badge bg-secondary">Terkirim</span>';   
                        }else if(data =='proses'){
                            roles='<span class="badge bg-warning">Proses</span>';   
                        }else if(data =='selesai'){
                            roles='<span class="badge bg-success">Selesai</span>';   
                        }else{
                            roles='<span class="badge bg-secondary">Tidak Diketahui</span>';   
                        }
                        
                        return roles;
                    }
                },
                {
                    data: 'id',name: 'id',orderable: false, searchable: false,
                    render:function (data, type, row) {
                        linkShow = urlShow.replace(':slug', data);
                        return '<a href="'+linkShow+'" class="btn btn-sm btn-primary"><i class="bx bx-pencil"></i> Lihat Pengaduan</a>';
                    }
                }

            ]
        });
    });
 

</script>
@endpush