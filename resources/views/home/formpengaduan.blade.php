@extends('layouts.main-home')
@section('section')
<div class="breadcrumbs">
    <div class="page-header d-flex align-items-center"
        style="background-image: url('{{ asset('theme-home/assets/img/page-header.jpg') }}');">
        <div class="container position-relative">
            <div class="row d-flex justify-content-center">
                <div class="col-lg-6 text-center">
                    <h2>Menu Pengaduan</h2>
                    <p>Data Pengaduan Anda.</p>
                </div>
            </div>
        </div>
    </div>
    <nav>
        <div class="container">
            <ol>
                <li><a href="index.html">Home</a></li>
                <li>Data Pengaduan Anda</li>
            </ol>
        </div>
    </nav>
</div><!-- End Breadcrumbs -->

<!-- ======= Contact Section ======= -->
<section id="contact" class="contact">
    <div class="container" data-aos="fade-up">

        <div class="row gy-4 mt-4">

            <div class="col-lg-4">

                <div class="info-item d-flex">
                    <i class="bi bi-geo-alt flex-shrink-0"></i>
                    <div>
                        <h4>Alamat:</h4>
                        <p>Kantor Desa Keplaksari Kecamatan Peterongan</p>
                    </div>
                </div><!-- End Info Item -->

                <div class="info-item d-flex">
                    <i class="bi bi-envelope flex-shrink-0"></i>
                    <div>
                        <h4>Email:</h4>
                        <p>info@peterongan.com</p>
                    </div>
                </div><!-- End Info Item -->

                <div class="info-item d-flex">
                    <i class="bi bi-phone flex-shrink-0"></i>
                    <div>
                        <h4>Call:</h4>
                        <p>+62859879908</p>
                    </div>
                </div><!-- End Info Item -->

            </div>

            <div class="col-lg-8">
                <form action="{{ route('home.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group mt-3">
                        <input type="text" value="{{ old('judul_pengaduan') }}" class="form-control"
                            name="judul_pengaduan" id="judul_pengaduan" placeholder="Judul" required>
                        @error('judul_pengaduan')
                        <div class="alert alert-danger mt-2">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group mt-3">
                        <input type="file" value="{{ old('foto_pengaduan') }}" placeholder="Foto Pengaduan"
                            class="form-control @error('foto_pengaduan') is-invalid @enderror" name="foto_pengaduan"
                            accept="image/*">
                        @error('foto_pengaduan')
                        <div class="alert alert-danger mt-2">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group mt-3">
                        <textarea class="form-control" name="isi_pengaduan" rows="5" placeholder="Isi Pengaduan"
                            required>{{ old('isi_pengaduan') }}</textarea>
                        @error('isi_pengaduan')
                        <div class="alert alert-danger mt-2">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <hr>
                    <div class="text-center"><button class="btn btn-primary" type="submit">Adukan</button></div>
                </form>
            </div><!-- End Contact Form -->

        </div>

    </div>
</section><!-- End Contact Section -->

@endsection

@push('scripts')
<script>

</script>
@endpush