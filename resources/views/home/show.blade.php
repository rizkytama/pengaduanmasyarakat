@extends('layouts.main-home')
@section('section')
<div class="breadcrumbs">
    <div class="page-header d-flex align-items-center"
        style="background-image: url('{{ asset('theme-home/assets/img/page-header.jpg') }}');">
        <div class="container position-relative">
            <div class="row d-flex justify-content-center">
                <div class="col-lg-6 text-center">
                    <h2>Rincian Pengaduan</h2>
                    <p>Data Rincian Pengaduan Anda.</p>
                </div>
            </div>
        </div>
    </div>
    <nav>
        <div class="container">
            <ol>
                <li><a href="index.html">Home</a></li>
                <li>Data Pengaduan Anda</li>
            </ol>
        </div>
    </nav>
</div><!-- End Breadcrumbs -->

<!-- ======= Contact Section ======= -->
<section id="contact" class="contact">
    <div class="container" data-aos="fade-up">

        <div class="row gy-4 mt-4">

            <div class="col-lg-4">

                <div class="info-item d-flex">
                    <i class="bi bi-geo-alt flex-shrink-0"></i>
                    <div>
                        <h4>Alamat:</h4>
                        <p>Kantor Desa Keplaksari Kecamatan Peterongan</p>
                    </div>
                </div><!-- End Info Item -->

                <div class="info-item d-flex">
                    <i class="bi bi-envelope flex-shrink-0"></i>
                    <div>
                        <h4>Email:</h4>
                        <p>info@peterongan.com</p>
                    </div>
                </div><!-- End Info Item -->

                <div class="info-item d-flex">
                    <i class="bi bi-phone flex-shrink-0"></i>
                    <div>
                        <h4>Call:</h4>
                        <p>+62859879908</p>
                    </div>
                </div><!-- End Info Item -->

            </div>

            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header">
                        PENGADUAN
                    </div>
                    <div class="card-body mt-3">
                        <table class="table">
                            <tr>
                                <th>STATUS PENGADUAN</th>
                                <th>:</th>
                                <td>
                                    @if($pengaduan->status_pengaduan =='0')
                                    <span class="badge bg-secondary">Terkirim</span>
                                    @elseif($pengaduan->status_pengaduan =='proses')
                                    <span class="badge bg-warning">Proses</span>
                                    @elseif($pengaduan->status_pengaduan =='selesai')
                                    <span class="badge bg-success">Selesai</span>
                                    @else
                                    <span class="badge bg-secondary">Tidak Diketahui</span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>TANGGAL PENGADUAN</th>
                                <th>:</th>
                                <td>{{ $pengaduan->tgl_pengaduan }}</td>
                            </tr>
                            <tr>
                                <th>JUDUL PENGADUAN</th>
                                <th>:</th>
                                <td>{{ $pengaduan->judul_pengaduan }}</td>
                            </tr>
                            <tr>
                                <th>ISI PENGADUAN</th>
                                <th>:</th>
                                <td>{{ $pengaduan->isi_pengaduan }}</td>
                            </tr>
                            <tr>
                                <th>FOTO PENGADUAN</th>
                                <th>:</th>
                                <td><img src="{{ url('storage/foto_pengaduan').'/'.$pengaduan->foto_pengaduan }}"
                                        alt=""></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <hr>
                <div class="card">
                    <div class="card-header">
                        TANGGAPAN
                    </div>
                    <div class="card-body mt-3">
                        <table class="table">
                            <tr>
                                <th>TANGGAL TANGAPAN</th>
                                <th>:</th>
                                <td>{{ $pengaduan->tgl_tanggapan }}</td>
                            </tr>
                            <tr>
                                <th>ISI TANGAPAN</th>
                                <th>:</th>
                                <td>{{ $pengaduan->isi_tanggapan }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div><!-- End Contact Form -->

        </div>

    </div>
</section><!-- End Contact Section -->

@endsection

@push('scripts')
<script>

</script>
@endpush