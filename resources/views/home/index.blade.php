@extends('layouts.main-home')
@section('section')
<!-- ======= Hero Section ======= -->
<section id="hero" class="hero d-flex align-items-center">
    <div class="container">
        <div class="row gy-4 d-flex justify-content-between">
            <div class="col-lg-6 order-2 order-lg-1 d-flex flex-column justify-content-center">
                <h2 data-aos="fade-up">{{ env('NAMA_APP') }}</h2>
                <p data-aos="fade-up" data-aos-delay="100">Cek Pengaduan Anda Dengan Mengisi Kode Pengaduan Anda
                    Dibawah Ini</p>

                {{-- <form action="#" class="form-search d-flex align-items-stretch mb-3" data-aos="fade-up"
                    data-aos-delay="200">
                    <input type="text" class="form-control" placeholder="Masukkan Kode Pengaduan">
                    <button type="submit" class="btn btn-primary">Cek Status Aduan</button>
                </form> --}}
            </div>

            <div class="col-lg-5 order-1 order-lg-2 hero-img" data-aos="zoom-out">
                <img src="{{ asset('theme-home/assets/img/hero-img.svg') }}" class="img-fluid mb-3 mb-lg-0" alt="">
            </div>

        </div>
    </div>
</section><!-- End Hero Section -->
@endsection

@push('scripts')
<script>

</script>
@endpush