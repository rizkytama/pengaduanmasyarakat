<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InstansiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'nama_instansi'     => 'required',
            'alamat_instansi'   => 'required|min:1'
        ];
    }

    public function messages()
    {
        return [
            'nama_instansi.required' => 'Nama Instansi Harus Diisi!',
            'alamat_instansi.required' => 'Alamat Instansi Harus Diisi!'
        ];
    }
}
