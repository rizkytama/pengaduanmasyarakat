<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubKategoriRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'kategori_id'     => 'required',
            'nama_subkategori'   => 'required|min:1',
            'keterangan_subkategori'   => 'required|min:1'
        ];
    }

    public function messages()
    {
        return [
            'kategori_id.required' => 'Kategori Harus Diisi!',
            'nama_subkategori.required' => 'Nama Subkategori Harus Diisi!',
            'keterangan_subkategori.required' => 'Keterangan Harus Diisi!'
        ];
    }
}
