<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'VirtualAccount' => 'required',
            'Amount'         => 'required',
            'Reference'      => 'required',
            'Tanggal'        => 'required'
        ];
    }

    public function messages()
    {
        return [
            'VirtualAccount.required' => 'Virtual Account Harus Diisi!',
            'Amount.required'         => 'Amount Harus Diisi!',
            'Reference.required'      => 'Reference Harus Diisi!',
            'Tanggal.required'        => 'Tanggal Harus Diisi!'
        ];
    }
}
