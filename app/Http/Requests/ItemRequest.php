<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'subkategori_id' => 'required',
            'nama_item'      => 'required|min:1',
            'spesifikasi'    => 'required|min:1',
            'harga'          => 'required|min:1',
            'cover'          => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }
}
