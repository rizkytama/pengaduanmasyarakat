<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KategoriRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'nama_kategori'     => 'required',
            'keterangan_kategori'   => 'required|min:1'
        ];
    }

    public function messages()
    {
        return [
            'nama_kategori.required' => 'Nama Kategori Harus Diisi!',
            'keterangan_kategori.required' => 'Keterangan Harus Diisi!'
        ];
    }
}
