<?php

namespace App\Http\Controllers;

use App\Models\Pengaduan;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $Pengaduan;

    public function __construct()
    {
        $this->Pengaduan = new Pengaduan;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home.index');
    }

    public function pengaduan()
    {
        if (request()->ajax()) {
            return $this->Pengaduan->getDatatable();
        }
        return view('home.pengaduan');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('home.formpengaduan');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // try{
        $this->validate($request, [
            'foto_pengaduan'  => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'judul_pengaduan' => 'required|min:5',
            'isi_pengaduan'   => 'required|min:10'
        ]);

        //upload image
        if ($request->hasFile('foto_pengaduan')) {
            $image = $request->file('foto_pengaduan');
            $image->storeAs('public/foto_pengaduan', $image->hashName());
            $cover = $image->hashName();
        } else {
            $cover = '';
        }

        //create post
        Pengaduan::create([
            'tgl_pengaduan'=>date('Y-m-d H:i:s'),
            'user_id'=>auth()->user()->id,
            'judul_pengaduan'=>$request->judul_pengaduan,
            'isi_pengaduan'=>$request->isi_pengaduan,
            'foto_pengaduan'=>$cover,
            'status_pengaduan'=>'0',
            'tgl_tanggapan'=>null,
            'isi_tanggapan'=>null,
            'petugas_id'=>null,
        ]);

        //redirect to index
        return redirect()->route('home.pengaduan')->with(['success' => 'Data Berhasil Disimpan!']);
    // } catch (\Exception $e) {
    //     dd($e);
    // }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Home  $home
     * @return \Illuminate\Http\Response
     */
    public function show(Pengaduan $pengaduan)
    {
        $data = [
            'pengaduan' => $pengaduan
        ];
        return view('home.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Home  $home
     * @return \Illuminate\Http\Response
     */
    public function edit(Home $home)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Home  $home
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Home $home)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Home  $home
     * @return \Illuminate\Http\Response
     */
    public function destroy(Home $home)
    {
        //
    }
}
