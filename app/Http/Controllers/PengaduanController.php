<?php

namespace App\Http\Controllers;

use App\Models\Pengaduan;
use Illuminate\Http\Request;

class PengaduanController extends Controller
{
    private $title = 'Pengaduan';
    private $icon = '<i class="bx bx-grid"></i>';
    protected $Pengaduan;
    public function __construct()
    {
        $this->Pengaduan = new Pengaduan;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->Pengaduan->getDatatableProses();
        }
        $data = [
            'title' => $this->title,
            'icon' => $this->icon,
            'active' => 'datapengaduan'
        ];
        return view('pengaduan.index',$data);
    }
    public function verifpengaduan()
    {
        if (request()->ajax()) {
            return $this->Pengaduan->getDatatableVerif();
        }
        $data = [
            'title' => $this->title,
            'icon' => $this->icon,
            'active' => 'verifdatapengaduan'
        ];
        return view('pengaduan.verifindex',$data);
    }
    public function aksiverif(Pengaduan $pengaduan)
    {
        $pengaduan->update([
            'status_pengaduan'     => 'proses',
        ]);
        return redirect(url()->previous())->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pengaduan  $pengaduan
     * @return \Illuminate\Http\Response
     */
    public function show(Pengaduan $pengaduan)
    {
        $data = [
            'title' => $this->title,
            'icon' => $this->icon,
            'active' => 'verifdatapengaduan',
            'pengaduan' => $pengaduan
        ];
        return view('pengaduan.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pengaduan  $pengaduan
     * @return \Illuminate\Http\Response
     */
    public function edit(Pengaduan $pengaduan)
    {
        $data = [
            'title' => $this->title,
            'icon' => $this->icon,
            'active' => 'datapengaduan',
            'pengaduan' => $pengaduan
        ];
        return view('pengaduan.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pengaduan  $pengaduan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pengaduan $pengaduan)
    {
        $this->validate($request, [
            'isi_tanggapan'   => 'required|min:10'
        ]);
        $pengaduan->update([
            'isi_tanggapan' => $request->isi_tanggapan,
            'tgl_tanggapan' => date('Y-m-d H:i:s'),
            'petugas_id'    => auth()->user()->id,
            'status_pengaduan'    => 'selesai',
        ]);
        return redirect()->route('pengaduan.list')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pengaduan  $pengaduan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pengaduan $pengaduan)
    {
        //
    }
}
