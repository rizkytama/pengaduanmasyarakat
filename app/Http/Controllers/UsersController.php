<?php

namespace App\Http\Controllers;

use App\Models\User;
use Spatie\Permission\Models\Role;
use App\Http\Requests\UsersStoreRequest;
use App\Http\Requests\UsersUpdatePasswordRequest;
use App\Http\Requests\UsersRoleRequest;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpKernel\Exception\HttpException;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UsersController extends Controller
{

    protected $UserModel;
    private $title = 'Data Users';
    private $active = 'datausers';
    private $icon = '<i class="bx bx-user-circle"></i>';
    public function __construct()
    {
        $this->UserModel = new User;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->UserModel->getDatatable();
        }
        $data = [
            'title' => 'List ' . $this->title,
            'icon' => $this->icon,
            'active' => $this->active,
        ];
        return view('users.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'title' => 'Tambah ' . $this->title,
            'icon' => $this->icon,
            'active' => $this->active,
        ];
        return view('users.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsersStoreRequest $request)
    {
        try {
            User::create([
                'nik'        => $request->nik,
                'name'        => $request->name,
                'telp'        => $request->telp,
                'email'       => $request->email,
                'password'    => Hash::make($request->password),
            ]);
        } catch (\Exception $e) {
            throw new HttpException(500, $e->getMessage());
        }
        //redirect to index
        return redirect()->route('users.list')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $users)
    {
        $roles = Role::get();
        $data = [
            'roles' => $roles,
            'title'  => 'Ubah ' . $this->title,
            'icon'   => $this->icon,
            'active' => $this->active,
            'user'  => $users,
            'roleUser'  => collect($users->roles),
        ];
        return view('users.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $kategori
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $users)
    {
        $this->validate($request, [
            'name'  => 'required|min:1',
            'email' => [
                'required',
                Rule::unique('users')->ignore($users->id),
            ],
        ]);
        try {
            $data = [
                'name'        => $request->name,
                'email'       => $request->email,
            ];
            $this->UserModel->actionUpdate($users, $data);
        } catch (\Exception $e) {
            throw new HttpException(500, $e->getMessage());
        }
        //redirect to index
        return redirect()->route('users.list')->with(['success' => 'Data Berhasil Diubah!']);
    }

    public function updatePassword(UsersUpdatePasswordRequest $request, User $users)
    {
        try {
            $data = [
                'password' => Hash::make($request->password),
            ];
            // dd($data);
            $this->UserModel->actionUpdate($users, $data);
        } catch (\Exception $e) {
            throw new HttpException(500, $e->getMessage());
        }
        //redirect to index
        return redirect()->route('users.list')->with(['success' => 'Password Berhasil Diubah!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $users)
    {
        try {
            $this->UserModel->actionDelete($users);
        } catch (\Exception $e) {
            throw new HttpException(500, $e->getMessage());
        }
        //redirect to index
        return redirect()->route('users.list')->with(['success' => 'Data Berhasil Dihapus!']);
    }
    public function changeRole(User $users, UsersRoleRequest $request)
    {
        try {
            $users->syncRoles($request->role);
        } catch (\Exception $e) {
            throw new HttpException(500, $e->getMessage());
        }
        //redirect to index
        return redirect()->route('users.list')->with(['success' => 'Data Berhasil Dihapus!']);
    }
}
