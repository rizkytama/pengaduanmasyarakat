<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    private $title = 'Dashboard';
    private $icon = '<i class="bx bx-grid"></i>';
    public function __construct()
    {
        
    }

    public function index()
    {
        $data = [
            'title' => $this->title,
            'icon' => $this->icon,
            'active' => 'dashboard'
        ];
        return view('dashboard.index', $data);
    }
    public function dataTableTrxTerbaru()
    {
        if (request()->ajax()) {
            return $this->TransaksiModel->getdataTableTrxTerbaru();
        }
    }
    public function dataTableItemRetribusiPalingBanyak()
    {
        // if (request()->ajax()) {
        return $this->TransaksiDetailModel->getDataTableItemRetribusiPalingBanyak();
        // }
    }
}
