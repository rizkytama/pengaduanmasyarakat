<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pengaduan extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'pengaduan';
    protected $dates = ['deleted_at'];
    public $incrementing = false;
    protected $fillable = [
        'tgl_pengaduan',
        'user_id',
        'judul_pengaduan',
        'isi_pengaduan',
        'foto_pengaduan',
        'status_pengaduan',
        'tgl_tanggapan',
        'isi_tanggapan',
        'petugas_id',
    ];
    // protected $with = ['masyarakat'];

    public function masyarakat()
    {
        return $this->hasOne(User::class, 'user_id', 'id')->withTrashed();
    }

    public function getDatatable()
    {
        $model = Pengaduan::orderByDesc('created_at');
        if (auth()->user()->hasRole('masyarakat')) {
            $model->where('user_id', auth()->user()->id);
        }
        return DataTables::of($model)
        ->addColumn('foto_pengaduan', function ($model) {
            if ($model->foto_pengaduan) {
                $foto_pengaduan = url('storage/foto_pengaduan') . '/' . $model->foto_pengaduan;
            } else {
                $foto_pengaduan = url('/storage/img/img.png');
            }
            return $foto_pengaduan;
        })
        ->addIndexColumn()->make();
    }
    public function getDatatableVerif()
    {
        $model = Pengaduan::orderByDesc('created_at')->where('status_pengaduan', '0');
        return DataTables::of($model)
        ->addColumn('foto_pengaduan', function ($model) {
            if ($model->foto_pengaduan) {
                $foto_pengaduan = url('storage/foto_pengaduan') . '/' . $model->foto_pengaduan;
            } else {
                $foto_pengaduan = url('/storage/img/img.png');
            }
            return $foto_pengaduan;
        })
        ->addIndexColumn()->make();
    }
    public function getDatatableProses()
    {
        $model = Pengaduan::orderByDesc('created_at')->where('status_pengaduan','!=','0');
        return DataTables::of($model)
        ->addColumn('foto_pengaduan', function ($model) {
            if ($model->foto_pengaduan) {
                $foto_pengaduan = url('storage/foto_pengaduan') . '/' . $model->foto_pengaduan;
            } else {
                $foto_pengaduan = url('/storage/img/img.png');
            }
            return $foto_pengaduan;
        })
        ->addIndexColumn()->make();
    }
}
